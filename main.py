import os, nextcord, config,asyncio
from nextcord.ext import commands

intent = nextcord.Intents.default()
intent.members = True
React = commands.Bot(
    command_prefix=config.Bot_prefix,
    case_insensitive=True,
    help_command=None,
    intents=intent,
    strip_after_prefix=True,
)


async def loadcogs():
    for file in os.listdir("cogs"):
        if file.endswith(".py"):
            try:
                React.load_extension(f"cogs.{file[:-3]}")
                print(f"successfully load {file[:-3]}")
            except Exception as e:
                print(f"Unable to load {file[:-3]} {e}")


@React.event
async def on_connect():
    print("Connected to discord API")


@React.event
async def on_ready():
    print("Bot is online")


if __name__ == "__main__":
    asyncio.run(loadcogs())
    React.run(config.Bot_token, reconnect=True)
